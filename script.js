$(document).ready(function(){

/*Slick photos carousel*/
  $(document).ready(function () {
    $('.js-slick').slick({
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
      dots: true,
      draggable: true,
      fade: true,
      speed: 1500
    });
    
  });



/*Slick brands carousel*/
  $('.multiple-items').slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    speed: 1500,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        arrows: true,
        dots: false,
        centerMode: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        arrows: true,
        dots: false,
        centerMode: true
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        arrows: true,
        dots: false,
        centerMode: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

});